package vector

import "math"

type Vector struct {
	X, Y int
}

func New(x, y int) Vector {
	return Vector{X: x, Y: y}
}

func (v *Vector) Distance(target Vector) int {
	x := math.Abs(float64(target.X) - float64(v.X))
	y := math.Abs(float64(target.Y) - float64(v.Y))
	return int(x + y)
}

func (v *Vector) MoveTo(target Vector, distance int) Vector {
	dist := v.Distance(target)
	if dist < distance {
		return target
	}

	newDest := Vector{}
	// 1. get the angle with the hypotenuse
	// when triangle ABC where A = start point, B=target, C=right angle
	ab := float64(dist)
	ac := float64(v.Distance(Vector{v.X, target.Y}))
	cos := ac / ab

	// 2. calcul Y
	newAC := float64(distance) * cos

	if v.Y < target.Y { // target below the source
		newDest.Y = v.Y + int(newAC)
	} else { // target over the source
		newDest.Y = v.Y - int(newAC)
	}

	// 3. calcul X
	newBC := math.Sqrt(math.Pow(float64(distance), 2) - math.Pow(newAC, 2))

	if v.X < target.X { // target on the right
		newDest.X = v.X + int(newBC)
	} else { // target on the left
		newDest.X = v.X - int(newBC)
	}

	if newDest.X < 0 {
		newDest.X = 0
	}
	if newDest.Y < 0 {
		newDest.Y = 0
	}
	return newDest
}
func (v *Vector) GetXYspeed(target Vector, distance int) Vector {

	var vspeed Vector

	destination := v.MoveTo(target, distance)

	vspeed.X = destination.X - v.X
	vspeed.Y = destination.Y - v.Y

	return vspeed
}
func (v *Vector) InRange(target Vector, distance int) bool {
	return v.Distance(target) <= distance
}

func (v *Vector) Equals(target Vector) bool {
	return v.X == target.X && v.Y == target.Y
}

func (v *Vector) Symmetric(center Vector) Vector {
	return Vector{center.X*2 - v.X, center.Y*2 - v.Y}
}

func (v *Vector) WithinBounds(minx, miny, maxx, maxy int) bool {
	return v.X >= minx && v.X <= maxx && v.Y >= miny && v.Y <= maxy
}

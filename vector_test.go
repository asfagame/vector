package vector

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVspeed(t *testing.T) {
	pointA := Vector{0, 0}
	target := Vector{0, 1000}

	pointB := pointA.MoveTo(target, 200)
	pointB_expected := Vector{0, 200}

	assert.Equal(t, pointB_expected, pointB)

	vspeed_expected := Vector{0, 200}

	assert.Equal(t, vspeed_expected, pointA.GetXYspeed(target, 200))

}

func TestVectorInRange(t *testing.T) {
	pointA := Vector{0, 0}
	target := Vector{0, 100}

	assert.True(t, pointA.InRange(target, 500))
	assert.False(t, pointA.InRange(target, 5))

}

func TestVectorEquals(t *testing.T) {
	pointA := Vector{0, 0}
	assert.True(t, pointA.Equals(Vector{0, 0}))
	assert.False(t, pointA.Equals(Vector{0, 2}))
	assert.False(t, pointA.Equals(Vector{2, 2}))
	assert.False(t, pointA.Equals(Vector{2, 0}))
}

func TestVectorSymmetric(t *testing.T) {
	type test struct {
		source, destination Vector
	}
	center := Vector{2, 2}

	tests := []test{
		{
			source:      Vector{0, 0},
			destination: Vector{4, 4},
		}, {
			source:      Vector{4, 4},
			destination: Vector{0, 0},
		},
		{
			source:      Vector{0, 2},
			destination: Vector{4, 2},
		},
		{
			source:      Vector{2, 0},
			destination: Vector{2, 4},
		},
		{
			source:      Vector{2, 0},
			destination: Vector{2, 4},
		},
		{
			source:      Vector{3, 1},
			destination: Vector{1, 3},
		},
	}

	for _, te := range tests {
		assert.Equal(t, te.destination, te.source.Symmetric(center))
	}
}

func TestWithinBounds(t *testing.T) {
	type test struct {
		position Vector
		expected bool
	}
	minx := 0
	miny := 0

	maxx := 10
	maxy := 10

	tests := []test{
		{
			position: Vector{0, 0},
			expected: true,
		},
		{
			position: Vector{10, 10},
			expected: true,
		},
		{
			position: Vector{11, 10},
			expected: false,
		},
		{
			position: Vector{4, -1},
			expected: false,
		},
	}

	for _, te := range tests {
		assert.Equal(t, te.expected, te.position.WithinBounds(minx, miny, maxx, maxy))
	}

}
